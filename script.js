/* script for:

<script src="https://cdn.jsdelivr.net/npm/typed.js@2.0.12"></script>



*/
var Typed = new Typed (".hd", {
        strings: ["github.com/", "itzbandhan", "bandhan pokhrel"], // strings
        typeSpeed: 150, // type speed for your text
        backSpeed: 100, // back speed for your text
        loop: true, // sets the text loop to true and your text will start looping
})
    // note that all the copyright goes to ITZBANDHAN
    // © 2022 itzbandhan
    // https://github.com/itzbandhan
    // check out my first calculator app: https://itzbandhan.github.io/calculator
    // check out readme.md file for more information